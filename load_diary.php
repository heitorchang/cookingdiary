<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="style.css">
    <title>Cooking Diary</title>
  </head>
  <body>
    <script>
    <?= file_get_contents(__DIR__ . "/diary.js") ?>

    <?= file_get_contents(__DIR__ . "/ingredients.js") ?>
    </script>

    <?php include("header.php");
    date_default_timezone_set("America/Sao_Paulo");
    ?>

    <p>
        <strong>Date:</strong> <input id="date" type="date" value="<?= date('Y-m-d') ?>">
        <strong>Dish:</strong> <select id="dish">
            <option value="">(select a dish)</option>
        </select>
      <button onclick="process()">Add entry</button> (Remember to Save)
    </p>

    <form action="save_diary.php" method="POST">
      To reset: <code>let diary = {};</code><br>
      <textarea id="file_contents" name="contents" rows="6" cols="48"><?= file_get_contents(__DIR__ . "/diary.js") ?></textarea>
      <br>
      <input type="submit" value="Save file">
    </form>

    <br><br>
    <div>
      Existing entries
      <ul id="existing">
      </ul>
    </div>

    <script>
      // fill in 'existing' list
      const existingList = document.getElementById("existing");

      Object.keys(diary).forEach(key => {
        const li = document.createElement("li");

        const weekday = (new Date(key.substring(0, 10) + "T12:00")).getDay();
        const wkdy = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

        li.innerHTML = `${key.substr(0,10)} <code>${wkdy[weekday]}</code> ${diary[key]}`;
        existingList.appendChild(li);
      });

      // populate 'dish' select
      const dishSelect = document.getElementById("dish");

      Object.keys(ingredients).forEach(key => {
      const option = document.createElement("option");
      option.value = key;
      option.innerText = key;
      dishSelect.appendChild(option);
      });

      function process() {
        const date = document.getElementById("date").value.trim();
        const time = "<?= date("H:i:s") ?>";
        const dish = document.getElementById("dish").value.trim();

        const key = `${date}T${(new Date()).toISOString().substring(11, 19)}Z`;
        diary[key] = dish;

        diary = Object.keys(diary).sort().reverse().reduce(
          (obj, key) => {
            obj[key] = diary[key];
            return obj;
          }, {});

        document.getElementById("file_contents").value = `let diary = \n ${JSON.stringify(diary, null, 2)};`;
        // document.getElementById("date").value = "<?= date('Y-m-d') ?>";
        document.getElementById("dish").value = '';
        document.getElementById("dish").focus();
      }
    </script>
  </body>
</html>
