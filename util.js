function formatDate(t) {
  return `${t.substr(8, 2)}/${t.substr(5, 2)}/${t.substr(2, 2)}`;
}
