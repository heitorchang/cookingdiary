<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="style.css">
    <title>Cooking Diary</title>
  </head>
  <body>
    <script>
    <?= file_get_contents(__DIR__ . "/ingredients.js") ?>

    <?= file_get_contents(__DIR__ . "/diary.js") ?>
    </script>

    <?php include("header.php"); ?>

    <p>
        <strong>Dish:</strong> <input type="text" id="dish" autofocus>
        <strong>Ingr., comma-sep, empty deletes dish:</strong> <input type="text" id="ingr">
      <button onclick="process()">Add/update dish</button> (Remember to Save when done)
    </p>

    <form action="save_ingredients.php" method="POST">
      To reset: <code>let ingredients = {};</code><br>
      <textarea id="file_contents" name="contents" rows="6" cols="48"><?= file_get_contents(__DIR__ . "/ingredients.js") ?></textarea>
      <br><br>
      <input type="submit" value="Save file">
    </form>

    <br><br>
    <div>
      <h3>Existing dishes</h3>
      <ul id="existing">
      </ul>
    </div>

    <script>
    // fill in 'existing' list
    const existingList = document.getElementById("existing");

    const hist = histogram();
    const last = lastMade();

    Object.keys(ingredients).forEach(key => {
      const li = document.createElement("li");
      let lastKey = last[key] ?? 'NA';
      li.innerText = `${key} x${hist[key] ?? 0}`; // , last ${lastKey}`;
      existingList.appendChild(li);
    });

    function histogram() {
      // count occurrences of each dish in the diary, and sort descending
      const counter = {};

      Object.values(diary).forEach(v => {
        if (!counter.hasOwnProperty(v)) {
          counter[v] = 1;
        } else {
          counter[v] += 1;
        }
      });

      const sortedKeys = Object.keys(counter).sort().reverse(); // sort alphabetically, reversed to undo next sort
      sortedKeys.sort((a, b) => counter[a] - counter[b]).reverse(); // sort by count

      sortedKeys.forEach(k => {
        const entry = `${k}: ${counter[k]}`;
      });

      return counter;
    }

    function lastMade() {
       // get latest occurrence of a dish in the diary
       // const seen = new Set();
       const seenCount = {};
       const lastSeen = {};

      Object.keys(diary).forEach(k => {
        const dish = diary[k];
        if (!lastSeen.hasOwnProperty(dish)) {
          seenCount[dish] = 0;
          lastSeen[dish] = formatDate(k);
        }
        seenCount[dish]++;
        if (seenCount[dish] > 1 && seenCount[dish] <= 3) {
          lastSeen[dish] += `, ${formatDate(k)}`;
          console.log(lastSeen[dish]);
        }
      });

      // sort by name
      const sortedDishes = Object.keys(lastSeen).sort();
      sortedDishes.forEach(k => {
        const entry = `${k}: ${lastSeen[k]}`;
      });

      return lastSeen;
    }

    function process() {
      const dish = document.getElementById("dish").value.trim();
      const rawIngr = document.getElementById("ingr").value.trim();

      if (rawIngr === '') {
        delete ingredients[dish];
      } else {
        let ingr = rawIngr.split(',');
        ingr = ingr.map(e => e.trim());
        ingredients[dish] = ingr;
      }

      ingredients = Object.keys(ingredients).sort().reduce(
        (obj, key) => {
          obj[key] = ingredients[key];
          return obj;
        }, {});

      document.getElementById("file_contents").value = `let ingredients = \n ${JSON.stringify(ingredients, null, 2)};`;
      document.getElementById("dish").value = '';
      document.getElementById("ingr").value = '';
      document.getElementById("dish").focus();
    }
    </script>
  </body>
</html>
