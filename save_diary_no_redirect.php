<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="style.css">
    <title>Cooking Diary</title>
  </head>
  <body>
    <?php

    file_put_contents(__DIR__ . "/diary.js", $_POST['contents']);

    echo "Saved diary contents.";

    ?>
<p>
  <a href="load_diary.php">Edit again</a>
</p>

<p>
  <a href="index.php">Back to home</a>
</p>
  </body>
</html>
