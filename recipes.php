<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="style.css">
    <title>Cooking Diary</title>
  </head>
  <body>
    <?php include("header.php");
    date_default_timezone_set("America/Sao_Paulo");
    ?>

    <h3>Recipes</h3>

    <pre style="white-space: pre-wrap; word-wrap: break-word;">
Contents

4 cold brew, beans, smoothie
5 tomato honey chicken
5 kalbi marinade
7 hot chocolate
8 coq au vin (MASTER!)
9 mac 'n' cheese (MASTER!)
10 hijiki gohan
11 pad thai
11 miso shiru (MASTER!)
12 mushi pan
12 beef teriyaki
13 panang curry
14 plain muffin
15 chicken biryani
16 oatmeal
17 pasta bolognese (MASTER!)
17 lasagna
18 pastry cream (MASTER!)
19 stroganoff
19 salad dressing
20 corn cake
21 noodle soup (MASTER!)
22 pizza de Buu (MASTER!)
23 risoto de funghi
24 nitsuke
24 mushu pork
25 deep-dish pizza
26 vodka sauce pasta
28 potato salad
29 biscotti
30 chocolate chip cookies (MASTER!)
31 double chocolate chunk cookies (not good)
32 banana bread
32 biscuits
33 oatmeal raisin cookies
34 devil's food cake
35 potato chowder
36 chicken pot pie
37


4 COLD BREW

mix 100 g ground coffee in 1 L water. leave overnight and strain


4 BEANS

soak 500 g overnight. cover with water (two finger joints above the beans)


4 SMOOTHIE

blend total of 750 mL of frozen or fresh fruit and milk. sweeten with 2 or more tbsp sugar.


5 TOMATO HONEY CHICKEN

500 g cubed chicken (3-4 cm a side)
2 cubed med. tomatoes
1 med. carrot, half-moons
1 med. onion, 2 cm cubes
1 med. bell pepper, 2 cm cubes

saute onion
add chicken, saute all sides
add all veggies
add a little water, cover and simmer for 10 mins. stir once or twice
add sauce: 60 mL soy sauce, 60 mL sake, 30 mL honey.
boil for about 3 mins. to remove alcohol
mix 1 tbsp cornstarch in cold water. add to pan and cook until liquid thickens.


5 KALBI MARINADE

marinate overnight:

1 kg pork tenderloin (filet mignon)
120 ml soy sauce
30 g brown sugar
7 mL crushed garlic
2 scallions, sliced
30 mL sesame oil
1 mL black pepper


7 HOT CHOCOLATE

1 c milk
2 tsp cornstarch
100 g chocolate
pinch salt

set aside a little milk to mix with cornstarch

heat milk and chocolate

when chocolate is melted, add cornstarch.

when just boiling, it is done


8 COQ AU VIN (MASTER!)

dice 75 g bacon in small cubes (1/2 cm)
dice 700 g chicken in 3 cm cubes
slice onion in half-moons.
mince 4 cloves garlic
slice mushrooms
set aside 150 mL wine and 75 mL water
set aside bay leaf and thyme
prepare beurre manie with 20 g butter and 20 g flour

brown bacon. set aside
brown chicken. set aside
brown onion, garlic and mushroom. set aside
add wine and water
return chix. boil. simmer for 10 mins
return other ingredients.
thicken with beurre manie, a little at a time


9 MAC 'N' CHEESE (MASTER!)

150 g gouda
100 g mozzarella
60 g flour
60 g butter
1 liter milk
nutmeg
bay leaf
1/2 onion
pasta

make bechamel sauce (melt butter over med. heat), add flour and stir for 2 minutes. gradually add milk. add chopped onion, nutmeg and bay leaf. stir occasionally. after about 10 minutes, add chopped cheese.

boil pasta separately, add sauce on pasta.


10 HIJIKI GOHAN

1/2 kg motigome - glutinous rice (soak for several hours)
12 g hijiki (soak for 1 hour)
150 g meat (chicken, pork)
3 dried shiitake (soak, save water)
1 carrot
1 gobo
oil
4 tbsp sake
2 tbsp mirin
3 tbsp soy sauce
2 tbsp sugar
1 tsp salt

steam rice inside a cloth at high heat for 20 mins
stir-fry remaining ingredients for 10 mins (cook meat thoroughly)
mix everything and stir-fry for another 5 mins

Pressure cooker version:
2 c (180 mL each) motigome
2 c regular sticky rice
fill with water to level 3 1/2 then add remaining ingredients


11 PAD THAI

Scramble 3 eggs. Set aside.

Meanwhile, boil rice noodles according to package instructions, leaving them still a bit hard (while mixing the noodles with the sauce over heat, they will finish cooking). After straining the noodles, wash them with your hands with cold water.

If using shrimp, heat oil and stir-fry heads until browned. remove heads. stir fry shrimp in the oil. set aside.

In the main wok, stir-fry garlic (if using other meat, add it and cook thoroughly. Add vegetables if desired). Add noodles and sauce. Mix and cook until noodles are al dente.

Optional: add about 125 g bean sprouts at the end.

Add sliced green onions.

Chop peanuts in a blender. When serving, sprinkle on top of noodles.


11 MISO SHIRU (MASTER!)

Boil 400 mL water and 10 mL hondashi. add chopped veggies (radish, carrot) and cook 5 mins. Add wakame. mix 10 mL miso with a little water. add to soup and cook 1 min. add sliced green onion.


12 MUSHI PAN

1 c flour
2 tsp baking powder
3 tbsp sugar
1/8 tsp salt
2 eggs
4 tbsp milk
2 tbsp oil

whisk dry ingredients
whisk liquids
add dry to liquid little by little
fill tins
steam for 13 minutes


12 BEEF TERIYAKI

brown garlic, onion
brown 600 g strips of lean beef
add 50 mL water
80 g soy sauce
80 g sake
80 g honey

reduce, stirring occasionally


13 PANANG CURRY

700 g beef in strips
1 onion
1 green bell pepper
5 cloves garlic
1 inch ginger, grated
200 mL coconut milk (100 mL if using chicken)
1 tbsp red curry paste
2 tbsp peanut butter
1/2 tsp salt
1-1/2 tbsp soy sauce
1 tbsp sugar
basil
green onion

saute ginger and garlic
add onion
add bell pepper
set aside
saute beef
re-add veggies
add coconut milk and seasonings
simmer 10 mins
add chopped basil and green onion


14 PLAIN MUFFIN

166 g flour
83 g sugar
10 g baking powder
2 g salt
50 g egg, beaten
117 mL milk
67 g butter, melted

sift dry ingredients
combine liquids
add liquids to dry
pour halfway in greased tins
bake 20-30 mins at 200 deg. C


15 CHICKEN BIRYANI

marinate 700 g chix breast in cubes in 1/2 lime's juice, ground coriander seeds and 5 tbsp masala (or mix with 2-1/2 tbsp curry)

grate 5 cloves garlic and 2 inches ginger

chop 1 med. tomato
dice 1 carrot into 1-cm cubes, bell pepper into 2-cm cubes
dice 1 onion (small dice)

brown onion
brown carrot
add tomato, bell pepper, salt, ginger and garlic
add chix
add 3 bay leaves
add 500 mL rice, washed
add 750 mL water
cook in low heat for 20-25 mins (until rice is well cooked. add water if needed)


16 OATMEAL

200 g dry oats
add double the volume of water
count 3-4 minutes since turning on the heat


17 PASTA BOLOGNESE (MASTER!)

brown garlic
brown onion
brown ground beef
add 2 small cans elefante, 2x amount of water
add bay leaves, salt
simmer for 10 mins.
add water if needed


17 LASAGNA

bechamel: roux with 62 g butter, 62 g flour, 1 L milk
grate 1 small onion, add to mixture
add bay leaf

make bolognese sauce (see above)

put bechamel first in pan
then pasta
then bolognese
than ham and cheese
repeat, with bolognese second-to-last and ham and cheese last
bake for 40 mins at medium heat


18 PASTRY CREAM (MASTER!)

62 g sugar
500 mL milk
bring these to just a boil

beat 2 egg yolks and 1 egg
sift 62 g sugar and 40 g cornstarch
temper egg (add hot milk slowly)
add cornstarch and sugar to pot
bring to a boil

when thickened, remove from heat and mix 31 g butter
chill with plastic wrap directly on pastry cream to avoid forming a skin


19 STROGANOFF

1 cebola, 1 cenoura, pimentao
frango - cozinhar 10 mins com pouco de agua e tampado
coloca creme de leite

tempero: 3 colher de sopa de molho ingles, 1 de mostarda, 1 de ketchup, sal
apenas esquentar


19 SALAD DRESSING

lime juice, olive oil, ajinomoto, salt or shoyu, pepper


20 CORN CAKE

1 can corn
1/2 can veg. oil
2 tbsp requeijao
1 can milk
1 can milharina
1 can sugar
3 eggs
1 tbsp baking powder

mix in blender
grease and flour pan
bake at 180 deg. C for 35 mins


21 NOODLE SOUP (MASTER!)

brown onion
brown ground beef
brown carrots and pumpkin
add lots of water, tomato
add bay leaf, herbs, bouillon cubes
cover, boil 10 mins
add bell peppers
cover, boil 5 minutes
add bowtie noodles, cook according to package instructions. boil uncovered
add chopped kale 3 minutes before the end of noodles' cooking


22 PIZZA DE BUU (MASTER!)

you need enough active dry yeast for 1 lb flour. (see packet instructions). add 1 tsp sugar and the yeast to 1 c of warm water (110 deg F). it should feel just slightly warmer than your hand.

when the mixture is foaming (after a few minutes), add it plus 1 tbsp olive oil to 1 lb flour inside a large bowl.

mix with a fork at first. gradually add room temperature (or warm) water and keep mixing with your hands until an elastic ball is formed. it should just stick to your skin, and have a smooth surface. bits of flour should not be flaking off. if too much water was added, correct it by adding small amounts of dry flour. knead slightly (i do it for about a minute only). lightly oil the bowl, and leave the ball of dough in it. cover the entire bowl with plastic wrap.

find a slightly larger bowl or pan, and fill it with a few inches of warm water (about 120 deg. F) and carefully place the bowl with dough inside. the warm water helps the dough rise. you may also leave the bowl under the sun or inside a warm oven. i think the warm water method is the easiest.

wait for 45 mins to an hour, until the dough doubles. then, punch down the risen dough, reshape the dough into a ball, and replace the water with new, warm water. wait another 45-60 mins.

knead the dough slightly so it is easier to handle, then stretch the dough to the shape of your baking pan. usually i stretch it directly on the baking pan. if you do this, remember to spread oil on the pan first, otherwise the dough will stick and burn easily.

add tomato sauce, cheese, and toppings

bake at 500 deg. F on the lower rack for at least 12 minutes. I think the total time will depend on the amount of toppings added.


23 RISOTO DE FUNGHI

200 g arborio
700-1000 mL hot water
1 chicken bouillon cube
15 g funghi secchi
30 g butter
70 g parmesan
60 mL white wine
1 small onion, fine diced
parsley

soak the funghi in the 700 mL hot water for 30 mins. save the water, and mix the bouillon in it.

chop the funghi finely. do not use a wooden board.

saute onion in 25 g butter (save 5 g for the end).

add rice, mixing non-stop, until rice appears translucent (about 2 mins)

add wine, stir until absorbed completely

add funghi

add the broth bit-by-bit, stirring often. when liquid is mostly absorbed, add more liquid.

taste the rice when liquid is almost used entirely. you may need to add more hot water.

add 5 g butter, parmesan and parsley


24 NITSUKE

1-1/2 L water
120 mL soy sauce
2 tbsp hondashi
60 mL mirin
60 mL sake
inhame
konyaku
cenoura
carne
gobo
chikuwa


24 MUSHU PORK

soak wood ear
cut 500 g pork in thin strips.
slice jp. cucumber diagonally
dice garlic, slice ginger. scramble eggs w/ salt

brown garlic and ginger in sesame oil
add pork
add a bit of water. simmer covered for 8 minutes
add wood ear
add cucumber, stir-fry briefly
add eggs (already scrambled)
add sauce (3 tbsp soy sauce, 1 tbsp sake, 1 tbsp sugar)
reduce


25 DEEP-DISH PIZZA

dough:

400 g flour
60 g cornmeal
1-1/4 tsp salt
12 g sugar
7 g yeast
300 mL warm water
55 g butter, melted
olive oil for coating

melt butter, let cool 5 mins. combine all, knead 5 mins. rise for 1h. roll into 15-in x 12-in rectangle. roll it up on short side. cut in half, shape into balls, and rise in fridge for 1h.

form a circle larger than the pan, cut off excess after lining pan. add mozzarella cheese, then tomato sauce. bake at med. heat for 30 mins.


26 VODKA SAUCE PASTA

1 onion
4 garlic cloves
100 g parmesan
olive oil
130 g tomato paste
pepper flakes
60 mL vodka
175 mL heavy cream

prepare hot water for pasta.
brown finely chopped onion and garlic (about 5 mins)
add tomato paste, saute until browned (about 4 mins)
deglaze with vodka. reduce heat to low. cook for 5 mins
scoop 50 mL hot water and add to heavy cream. mix.
slowly incorporate cream to sauce
add another 200 mL hot water.
add parmesan.


28 POTATO SALAD

hard boil 3 eggs (cover eggs with cold water, leaving 5 cm. once water boils, lower heat and boil for 10 minutes).

boil a lot of water (cover pan). dice 3 med. potatoes and 2 med. carrots into 1-cm cubes. boil them for 15 mins. drain and add canned peas and corn.

season with 1 tbsp mustard, 1 tbsp vinegar, 1/2 cup mayo, 1 tsp salt. mix with sliced green onions.


29 BISCOTTI

Whisk together:
270 g flour
1-1/2 teaspoon baking powder
1/3 teaspoon salt

Beat at low speed:
150 g sugar
115 g butter, room temperature

beat in 2 eggs (room temperature), one at a time

add flour, mix until just blended

stir in filling (sliced almonds, chocolate chips, etc)

form dough into a 40 cm x 8 cm log and place on parchment paper

bake at 180 deg. C for 30 mins

cool for 30 mins

cut diagonally into 1-1/2 cm slices

bake slices for 10 mins, flip, and bake another 10 mins


30 CHOCOLATE CHIP COOKIES

for 12 cookies:

cream at low speed:
75 g butter (room temp.)
45 g white sugar
45 g brown sugar
2 g salt

mix at low speed:
1 egg (room temp.)
2 mL vanilla extract

sift together and mix until blended:
135 g flour
2 g baking soda = 1/2 tsp

mix
100 g chocolate chips

drop on parchment paper.
bake at 180 deg C for 10-12 mins


31 DOUBLE CHOCOLATE CHUNK COOKIES

(not good, too crumbly)


32 BANANA BREAD

mix with fork, then sift together:
250 g flour
100 g sugar
12 g baking powder (3 tsp)
2 g baking soda (1/2 tsp)
4 g salt (2/3 tsp)

mix:
2 eggs (room temp.)
250 g mashed bananas (about 2-1/2)
82 g melted butter

add liquids to dry ingredients. do not overmix.

blend
60 g chopped walnuts

bake at 190 deg C for 20-30 mins.


32 BISCUITS

sift:
100 g flour
2 g salt (1/3 tsp)
5 g sugar
6 g baking powder (1-1/2 tsp)

cut with
35 g cold butter
until resembling a coarse cornmeal texture

add to dry ingredients, do not overmix:
65 mL milk

knead by folding in half and rotating 90 degrees. repeat 6-10 times. dough shouldn't be sticky

roll to a 1-cm thick sheet
cut straight down, place on a greased sheet.
bake 15-20 mins at 200 deg C

33 OATMEAL RAISIN COOKIES

cream:
100 g butter, room temp.
160 g brown sugar
2 g salt (1/3 tsp)

mix:
1 egg, room temp.
5 mL vanilla extract
12 mL milk

sift:
150 g flour
6 g baking powder (1-1/2 tsp)
3 g baking soda (2/3 tsp)
2 g cinnamon (1/2 tsp)

mix:
125 g oats
100 g raisins

drop on parchment paper. bake at 190 deg. C for 10-12 mins
makes 18
*     *     *     *
   *     *     *
*     *     *     *
   *     *     *
*     *     *     *


34 DEVIL'S FOOD CAKE

sift
150 flour
25 cocoa
3 salt
4.5 bkg powder
3 bkg soda

add and mix for 8 mins, scraping sides occasionally.
90 butter, room temp.
150 sugar
100 milk, room temp.
2 vanilla

combine in a separate bowl, then add to batter in 3 parts
75 milk
100 eggs

bake at 180 deg C for about 20-25 mins


35 POTATO CHOWDER

62 salt pork/bacon: fine dice, render
80 onion: med. dice, stir-fry without browning
40 flour: make roux, cook 5 mins
800 mL chicken stock. whip, bring to a boil
400 potato, med. dice. simmer 10-15 mins
add 400 mL hot milk
add 60 mL cream
salt to taste


36 CHICKEN POT PIE


boil med. diced 2 med potatoes and 1 lg carrot, diced, for 12-14 mins

saute 1 sm. onion, 500 g chix in med cubes. simmer in a little water for 10 mins, covered. stir once or twice

make bechamel sauce: make light roux with 90 g butter and 90 g flour. add 1 L milk, 500 mL water, and 1 cube chix. bouillon. cook for 5 mins with bay leaf. add cooked ingredients and 1 can of peas. add 200 mL heavy cream.

crust: mix flour and lard. pour filling in pan and cover with crust. bake at 200 deg C for 20-30 mins. check often
    </pre>
  </body>
</html>
