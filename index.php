<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="style.css">
        <title>Cooking Diary</title>
    </head>
    <body>
        <?php include("header.php"); ?>

        <div>
            <ul id="existing">
            </ul>
        </div>

        <div id="sectionsWrapper">
        </div>

        <script>
         <?= file_get_contents(__DIR__ . "/ingredients.js") ?>

         <?= file_get_contents(__DIR__ . "/diary.js") ?>

         const existingList = document.getElementById("existing");

         const dates = Object.keys(diary);

         const wkdy = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

         dates.slice(0, 27).forEach(key => {
           const li = document.createElement("li");
           const weekday = (new Date(key.substring(0, 10) + "T12:00")).getDay();
           li.innerHTML = `<code>${formatDate(key)} ${wkdy[weekday]}</code> ${diary[key]}`;
           // do not add diary history
           // existingList.appendChild(li);
         });

         function buildSection(header, items, lastDict) {
           const div = document.createElement('div');
           const h = document.createElement('h3');
           h.innerText = header;
           div.appendChild(h);
           const ul = document.createElement('ul');
           items.forEach(item => {
             const li = document.createElement('li');
             let lastKey = lastDict[item] ?? 'NA';
             li.innerText = `${item}, made ${lastKey}`;
             ul.appendChild(li);
           });
           div.appendChild(ul);
           return div;
         }

         function lastMade() {
           // get latest occurrence of a dish in the diary
           // const seen = new Set();
           const seenCount = {};
           const lastSeen = {};

           Object.keys(diary).forEach(k => {
             const dish = diary[k];
             if (!lastSeen.hasOwnProperty(dish)) {
               seenCount[dish] = 0;
               lastSeen[dish] = formatDate(k);
             }
             seenCount[dish]++;
             if (seenCount[dish] > 1 && seenCount[dish] <= 3) {
               lastSeen[dish] += `, ${formatDate(k)}`;
               console.log(lastSeen[dish]);
             }
           });
           return lastSeen;
         }

         function dishesWithIngredient() {
           // 1. list unique ingredients
           // 2. find all dishes containing it
           const ingrSections = {};

           Object.values(ingredients).forEach(lst => {
             lst.forEach(ingr => {
               if (!ingrSections.hasOwnProperty(ingr)) {
                 ingrSections[ingr] = [];
               }
             });
           });

           // iterate over dishes (ingredients keys) and add them to appropriate sections
           Object.keys(ingredients).forEach(dish => {
             ingredients[dish].forEach(ingr => {
               ingrSections[ingr].push(dish);
             });
           });

           return ingrSections;
         }

         const wrapper = document.getElementById('sectionsWrapper');
         const sections = dishesWithIngredient();
         const last = lastMade();

         Object.keys(sections).sort().forEach(section => {
           wrapper.appendChild(buildSection(section, sections[section], last));
         });

        </script>
    </body>
</html>
