<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="style.css">
        <title>Cooking Diary</title>
    </head>
    <body>
        <?php include("header.php"); ?>

        <div>
            <strong>Histogram</strong>
            <table style="width: auto;" id="histList">
            </table>
        </div>

        <script>
         <?= file_get_contents(__DIR__ . "/diary.js") ?>

         const histList = document.getElementById("histList");

         function histogram(last) {
           // count occurrences of each dish in the diary, and sort descending
           const counter = {};

           Object.values(diary).forEach(v => {
             if (!counter.hasOwnProperty(v)) {
               counter[v] = 1;
             } else {
               counter[v] += 1;
             }
           });

           const sortedKeys = Object.keys(counter).sort().reverse(); // sort alphabetically, reversed to undo next sort
           sortedKeys.sort((a, b) => counter[a] - counter[b]).reverse(); // sort by count

           sortedKeys.forEach(k => {
             const row = document.createElement("tr");
             row.innerHTML = `<td>${k}</td> <td>${counter[k]}</td> <td>${formatDate(last[k])}</td>`;
             histList.appendChild(row);
           });
         }

         function lastMade() {
           // get latest occurrence of a dish in the diary
           const seen = new Set();

           const lastSeen = {};

           Object.keys(diary).forEach(k => {
             const dish = diary[k];
             if (!lastSeen.hasOwnProperty(dish)) {
               lastSeen[dish] = k;
             }
           });

           // sort by name
           const sortedDishes = Object.keys(lastSeen).sort();
           sortedDishes.forEach(k => {
             const entry = `${k}: ${lastSeen[k]}`;
             console.log(entry);
           });

           return lastSeen;
         }

         const last = lastMade();

         histogram(last);
        </script>
    </body>
</html>
