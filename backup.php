<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="style.css">
    <title>Cooking Diary</title>
  </head>
  <body>
    <?php include("header.php"); ?>

    <textarea rows="20" cols="48"><?= file_get_contents(__DIR__ . "/ingredients.js") ?>


<?= file_get_contents(__DIR__ . "/diary.js") ?></textarea>
  </body>
</html>
